package main

import (
    "log"
    "net/http"
    "petstore/internal/tabler"
    "os"
    "petstore/internal/entities"
    "time"
    "os/signal"
    "syscall"
    "github.com/joho/godotenv"
    "context"
    "petstore/internal/router"
    "petstore/internal/controller"
    "petstore/internal/repository/pet"
    "petstore/internal/repository/store"
    "petstore/internal/repository/user"
    "fmt"
    "github.com/jmoiron/sqlx"
    _ "github.com/lib/pq"
)

func main() {
    err := godotenv.Load(".env")
    if err != nil {
        log.Fatal("Ошибка при загрузке файла .env")
    }
    db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
        os.Getenv("DB_USER"),
        os.Getenv("DB_PASSWORD"),
        os.Getenv("DB_HOST"),
        os.Getenv("DB_PORT"),
        os.Getenv("DB_NAME"),
    ))
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    var generator tabler.SQLiteGenerator
    m := tabler.NewMigrator(db, &generator)
    err = m.Migrate(&entities.User{}, &entities.Pet{}, &entities.Order{})
    if err != nil {
        panic(err)
    }

    userRepo := user.NewPostgresUserRepository(db)
    storeRepo := store.NewPostgresStoreRepository(db)
    petRepo := pet.NewPostgresPetRepository(db)
    ctrl := controller.NewController(&petRepo, &storeRepo, &userRepo)
    r := router.NewApiRouter(&ctrl)

    server := &http.Server{
        Addr:         ":8080",
        Handler:      r,
        ReadTimeout:  10 * time.Second,
        WriteTimeout: 10 * time.Second,
    }

    go func() {
        log.Println("Starting server...")
        if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
            log.Fatalf("Server error: %v", err)
        }
    }()

    sigChan := make(chan os.Signal, 1)
    signal.Notify(sigChan, syscall.SIGINT)

    <-sigChan

    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    err = server.Shutdown(ctx)
    if err != nil {
        log.Fatalf("Server shutdown error: %v", err)
    }

    <-ctx.Done()

    log.Println("Server stopped gracefully")
}
