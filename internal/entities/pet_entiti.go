package entities

type Pet struct {
    ID     int64  `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY"`
    Name   string `json:"name" db:"name" db_type:"VARCHAR(255)"`
    Status string `json:"status" db:"status" db_type:"VARCHAR(255)"`
}

func (p Pet) TableName() string {
    return "pets"
}
