package entities

type User struct {
    ID         int64  `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY"`
    Username   string `json:"username" db:"username" db_type:"VARCHAR(255)"`
    FirstName  string `json:"firstName" db:"first_name" db_type:"VARCHAR(255)"`
    LastName   string `json:"lastName" db:"last_name" db_type:"VARCHAR(255)"`
    Email      string `json:"email" db:"email" db_type:"VARCHAR(255)"`
    Password   string `json:"password" db:"password" db_type:"VARCHAR(255)"`
    Phone      string `json:"phone" db:"phone" db_type:"VARCHAR(255)"`
    UserStatus int64  `json:"userStatus" db:"user_status" db_type:"INTEGER"`
    Token      string `json:"token" db:"token" db_type:"VARCHAR(255)"`
}

func (u User) TableName() string {
    return "users"
}
