package controller

import (
    "petstore/internal/service/pet"
    "petstore/internal/service/store"
    "petstore/internal/service/user"
    pet2 "petstore/internal/repository/pet"
    store2 "petstore/internal/repository/store"
    user2 "petstore/internal/repository/user"
)

type Controllers struct {
    PetCtrl   pet.PetAPI
    StoreCtrl store.StAPI
    User      user.UserAPI
}

func NewController(petRepo pet2.PetRepository, storeRepo store2.StoreRepository, userRepo user2.UserRepository) Controllers {
    return Controllers{
        PetCtrl:   pet.NewPetController(petRepo),
        StoreCtrl: store.NewStoreController(storeRepo),
        User:      user.NewUserController(userRepo),
    }
}
