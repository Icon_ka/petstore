package tabler

import (
    "reflect"
)

type Tabler interface {
    TableName() string
}

type StructInfo struct {
    Fields   []string
    Pointers []interface{}
}

func GetStructInfo(u interface{}, args ...func(*[]reflect.StructField)) StructInfo {
    val := reflect.ValueOf(u).Elem()
    var structFields []reflect.StructField

    for i := 0; i < val.NumField(); i++ {
        structFields = append(structFields, val.Type().Field(i))
    }

    for i := range args {
        if args[i] == nil {
            continue
        }
        args[i](&structFields)
    }

    var res StructInfo

    for _, field := range structFields {
        valueField := val.FieldByName(field.Name)
        res.Pointers = append(res.Pointers, valueField.Addr().Interface())
        res.Fields = append(res.Fields, field.Tag.Get("db"))
    }

    return res
}

func FilterByFields(fields ...int) func(fields *[]reflect.StructField) {
    return func(f *[]reflect.StructField) {
        var filtered []reflect.StructField
        for _, field := range *f {
            for _, num := range fields {
                if num == field.Index[0] {
                    filtered = append(filtered, field)
                    break
                }
            }
        }
        *f = filtered
    }
}

func FilterByTags(tags map[string]func(value string) bool) func(fields *[]reflect.StructField) {
    return func(f *[]reflect.StructField) {
        var filtered []reflect.StructField
        for _, field := range *f {
            for tag, filter := range tags {
                if value, ok := field.Tag.Lookup(tag); ok && filter(value) {
                    filtered = append(filtered, field)
                    break
                }
            }
        }
        *f = filtered
    }
}
