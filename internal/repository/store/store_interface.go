package store

import (
    "context"
    "petstore/internal/entities"
)

type StoreRepository interface {
    Create(ctx context.Context, order entities.Order) error
    GetByID(ctx context.Context, id int64) ([]entities.Order, error)
    DeleteByID(ctx context.Context, id int64) error
}
