package store

import (
    "petstore/internal/entities"
    "context"
    "sync"
    sq "github.com/Masterminds/squirrel"
    "github.com/jmoiron/sqlx"
    "reflect"
)

type PostgresStoreRepository struct {
    db         *sqlx.DB
    SqlBuilder sq.StatementBuilderType
    sync.Mutex
}

func NewPostgresStoreRepository(db *sqlx.DB) PostgresStoreRepository {
    builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
    return PostgresStoreRepository{
        db:         db,
        SqlBuilder: builder,
    }
}

func (p PostgresStoreRepository) Create(ctx context.Context, order entities.Order) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    v := reflect.ValueOf(&order).Elem()

    values := []interface{}{}
    columns := []string{}
    for i := 0; i < v.NumField(); i++ {
        field := v.Type().Field(i)
        tag := field.Tag.Get("db")
        if tag == "id" || field.Name == "ID" {
            continue
        }
        values = append(values, v.Field(i).Interface())
        columns = append(columns, tag)
    }

    q, _, _ := p.SqlBuilder.Insert(order.TableName()).Columns(columns...).Values(values...).ToSql()
    _, err := p.db.ExecContext(ctx, q, values...)
    if err != nil {
        return err
    }

    return nil
}

func (p PostgresStoreRepository) GetByID(ctx context.Context, id int64) ([]entities.Order, error) {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    var findOrder []entities.Order

    sql, args, err := p.SqlBuilder.Select("*").From("orders").Where(sq.Eq{"id": id}).ToSql()
    if err != nil {
        return []entities.Order{}, err
    }

    err = p.db.SelectContext(ctx, &findOrder, sql, args...)
    if err != nil {
        return []entities.Order{}, err
    }

    return findOrder, nil
}

func (p PostgresStoreRepository) DeleteByID(ctx context.Context, id int64) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    sql, args, err := p.SqlBuilder.Delete("orders").Where(sq.Eq{"id": id}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, args...)
    if err != nil {
        return err
    }
    return nil
}
