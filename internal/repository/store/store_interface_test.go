package store

import (
    "testing"
    sqlmock "github.com/zhashkevych/go-sqlxmock"
    "petstore/internal/entities"
    "log"
    "context"
)

func TestPostgresStoreRepository_Create(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()
    order := entities.Order{
        ID:       1,
        PetID:    2,
        Quantity: 32131,
        ShipDate: "dasd",
        Status:   "dsad",
        Complete: false,
    }

    mock.ExpectExec("^INSERT INTO orders").WithArgs(order.PetID, order.Quantity, order.ShipDate, order.Status, order.Complete).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresStoreRepository(db)

    err = p.Create(ctx, order)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresStoreRepository_DeleteByID(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()

    mock.ExpectExec("^DELETE FROM orders").WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresStoreRepository(db)

    err = p.DeleteByID(ctx, 1)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresStoreRepository_GetByID(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        t.Fatalf("An error occurred while creating mock: %s", err)
    }
    defer db.Close()

    ctx := context.Background()

    rows := sqlmock.NewRows([]string{"id", "pet_id", "quantity", "ship_date", "status", "complete"}).
        AddRow(1, 1, 3000, "a", "teststatus", false)
    mock.ExpectQuery("^SELECT (.+) FROM orders WHERE id = ?").WithArgs(1).WillReturnRows(rows)

    p := NewPostgresStoreRepository(db)

    orders, err := p.GetByID(ctx, 1)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if len(orders) != 1 {
        t.Fatalf("Expected 1 order, got %d", len(orders))
    }

    if orders[0].ID != 1 || orders[0].PetID != 1 || orders[0].Status != "teststatus" || orders[0].Quantity != 3000 {
        t.Fatalf("Expected order with ID 1, name 'test', and status 'teststatus', got %+v", orders[0])
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}
