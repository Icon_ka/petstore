package repository

import (
    "petstore/internal/repository/pet"
    "petstore/internal/repository/store"
    "petstore/internal/repository/user"
    "github.com/jmoiron/sqlx"
)

type Repository struct {
    pet   pet.PetRepository
    store store.StoreRepository
    user  user.UserRepository
}

func NewRepository(db *sqlx.DB) Repository {
    return Repository{
        pet:   pet.NewPostgresPetRepository(db),
        store: store.NewPostgresStoreRepository(db),
        user:  user.NewPostgresUserRepository(db),
    }
}
