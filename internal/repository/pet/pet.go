package pet

import (
    "petstore/internal/entities"
    "context"
    "sync"
    sq "github.com/Masterminds/squirrel"
    "github.com/jmoiron/sqlx"
    "petstore/internal/tabler"
    "reflect"
)

type PostgresPetRepository struct {
    db         *sqlx.DB
    SqlBuilder sq.StatementBuilderType
    sync.Mutex
}

func NewPostgresPetRepository(db *sqlx.DB) PostgresPetRepository {
    builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
    return PostgresPetRepository{
        db:         db,
        SqlBuilder: builder,
    }
}

func (p PostgresPetRepository) Create(ctx context.Context, pet *entities.Pet) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    v := reflect.ValueOf(pet).Elem()

    values := []interface{}{}
    columns := []string{}
    for i := 0; i < v.NumField(); i++ {
        field := v.Type().Field(i)
        tag := field.Tag.Get("db")
        if tag == "id" || field.Name == "ID" {
            continue
        }
        values = append(values, v.Field(i).Interface())
        columns = append(columns, tag)
    }

    q, _, _ := p.SqlBuilder.Insert(pet.TableName()).Columns(columns...).Values(values...).ToSql()
    _, err := p.db.ExecContext(ctx, q, values...)
    if err != nil {
        return err
    }

    return nil
}

func (p PostgresPetRepository) Update(ctx context.Context, pet *entities.Pet) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    v := reflect.ValueOf(pet).Elem()

    fields := tabler.GetStructInfo(pet)
    updateValues := make(map[string]interface{})

    for i, field := range fields.Fields {
        if field == "ID" || reflect.DeepEqual(v.Field(i).Interface(), reflect.Zero(v.Field(i).Type()).Interface()) {
            continue
        }
        updateValues[field] = v.Field(i).Interface()
    }

    sql, values, err := p.SqlBuilder.Update(pet.TableName()).SetMap(updateValues).Where(sq.Eq{"id": pet.ID}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, values...)
    if err != nil {
        return err
    }

    return nil
}

func (p PostgresPetRepository) GetByStatus(ctx context.Context, status string) ([]entities.Pet, error) {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    var findPet []entities.Pet
    sql, args, err := p.SqlBuilder.Select("*").From("pets").Where(sq.Eq{"status": status}).ToSql()
    if err != nil {
        return []entities.Pet{}, err
    }

    err = p.db.SelectContext(ctx, &findPet, sql, args...)
    if err != nil {
        return []entities.Pet{}, err
    }

    return findPet, nil
}

func (p PostgresPetRepository) GetById(ctx context.Context, id int64) ([]entities.Pet, error) {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    var findPet []entities.Pet
    sql, args, err := p.SqlBuilder.Select("*").From("pets").Where(sq.Eq{"id": id}).ToSql()
    if err != nil {
        return []entities.Pet{}, err
    }

    err = p.db.SelectContext(ctx, &findPet, sql, args...)
    if err != nil {
        return []entities.Pet{}, err
    }

    return findPet, nil
}

func (p PostgresPetRepository) UpdateById(ctx context.Context, id int64, name, status string) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    sql, values, err := p.SqlBuilder.Update("pets").Set("name", name).Set("status", status).Where(sq.Eq{"id": id}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, values...)
    if err != nil {
        return err
    }

    return nil
}

func (p PostgresPetRepository) DeleteById(ctx context.Context, id int64) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()
    sql, args, err := p.SqlBuilder.Delete("pets").Where(sq.Eq{"id": id}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, args...)
    if err != nil {
        return err
    }
    return nil
}
