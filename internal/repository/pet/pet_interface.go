package pet

import (
    "context"
    "petstore/internal/entities"
)

type PetRepository interface {
    Create(ctx context.Context, pet *entities.Pet) error
    Update(ctx context.Context, pet *entities.Pet) error
    GetByStatus(ctx context.Context, status string) ([]entities.Pet, error)
    GetById(ctx context.Context, id int64) ([]entities.Pet, error)
    UpdateById(ctx context.Context, id int64, name, status string) error
    DeleteById(ctx context.Context, id int64) error
}
