package pet

import (
    "testing"
    sqlmock "github.com/zhashkevych/go-sqlxmock"
    "log"
    "petstore/internal/entities"
    "context"
)

func TestPostgresPetRepository_Create(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()
    pet := &entities.Pet{
        ID:     1,
        Name:   "test",
        Status: "teststatus",
    }

    mock.ExpectExec("^INSERT INTO pets").WithArgs(pet.Name, pet.Status).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresPetRepository(db)

    err = p.Create(ctx, pet)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresPetRepository_DeleteById(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()

    mock.ExpectExec("^DELETE FROM pets").WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresPetRepository(db)

    err = p.DeleteById(ctx, 1)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestGetById(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        t.Fatalf("An error occurred while creating mock: %s", err)
    }
    defer db.Close()

    ctx := context.Background()

    rows := sqlmock.NewRows([]string{"id", "name", "status"}).
        AddRow(1, "test", "teststatus")
    mock.ExpectQuery("^SELECT (.+) FROM pets WHERE id = ?").WithArgs(1).WillReturnRows(rows)

    p := NewPostgresPetRepository(db)

    pets, err := p.GetById(ctx, 1)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if len(pets) != 1 {
        t.Fatalf("Expected 1 pet, got %d", len(pets))
    }

    if pets[0].ID != 1 || pets[0].Name != "test" || pets[0].Status != "teststatus" {
        t.Fatalf("Expected pet with ID 1, name 'test', and status 'teststatus', got %+v", pets[0])
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresPetRepository_GetByStatus(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        t.Fatalf("An error occurred while creating mock: %s", err)
    }
    defer db.Close()

    ctx := context.Background()

    rows := sqlmock.NewRows([]string{"id", "name", "status"}).
        AddRow(1, "test", "teststatus")
    mock.ExpectQuery("^SELECT (.+) FROM pets WHERE status = ?").WithArgs("teststatus").WillReturnRows(rows)

    p := NewPostgresPetRepository(db)

    pets, err := p.GetByStatus(ctx, "teststatus")
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if len(pets) != 1 {
        t.Fatalf("Expected 1 pet, got %d", len(pets))
    }

    if pets[0].ID != 1 || pets[0].Name != "test" || pets[0].Status != "teststatus" {
        t.Fatalf("Expected pet with ID 1, name 'test', and status 'teststatus', got %+v", pets[0])
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestUpdateById(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        t.Fatalf("An error occurred while creating mock: %s", err)
    }
    defer db.Close()

    ctx := context.Background()

    mock.ExpectExec("^UPDATE pets").WithArgs("newname", "newstatus", 1).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresPetRepository(db)

    err = p.UpdateById(ctx, 1, "newname", "newstatus")
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresPetRepository_Update(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()
    pet := &entities.Pet{
        ID:     1,
        Name:   "test",
        Status: "teststatus",
    }

    mock.ExpectExec("^UPDATE pets").WithArgs(pet.ID, pet.Name, pet.Status, pet.ID).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresPetRepository(db)

    err = p.Update(ctx, pet)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}
