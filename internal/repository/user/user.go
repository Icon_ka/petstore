package user

import (
    "petstore/internal/entities"
    "context"
    "sync"
    "github.com/jmoiron/sqlx"
    sq "github.com/Masterminds/squirrel"
    "petstore/internal/tabler"
    "reflect"
    "fmt"
)

type PostgresUserRepository struct {
    db         *sqlx.DB
    SqlBuilder sq.StatementBuilderType
    sync.Mutex
}

func NewPostgresUserRepository(db *sqlx.DB) PostgresUserRepository {
    builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

    return PostgresUserRepository{
        db:         db,
        SqlBuilder: builder,
    }
}

func (p PostgresUserRepository) CreateWithArray(ctx context.Context, users []*entities.User) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    fields := tabler.GetStructInfo(&entities.User{}).Fields

    query := p.SqlBuilder.Insert("users").Columns(fields...)

    for _, user := range users {
        query = query.Values(user.Username, user.FirstName, user.LastName, user.Email, user.Password, user.Phone, user.UserStatus)
    }

    sql, _, err := query.ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql)
    if err != nil {
        return err
    }

    return nil
}

func (p PostgresUserRepository) GetByUsername(ctx context.Context, username string) ([]entities.User, error) {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    var findUser []entities.User
    sql, args, err := p.SqlBuilder.Select("*").From("users").Where(sq.Eq{"username": username}).ToSql()
    if err != nil {
        return []entities.User{}, err
    }

    fmt.Println(sql)
    err = p.db.SelectContext(ctx, &findUser, sql, args...)
    if err != nil {
        fmt.Println(err)
        return []entities.User{}, err
    }

    return findUser, nil
}

func (p PostgresUserRepository) UpdateByUsername(ctx context.Context, username string, updateData *entities.User) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    v := reflect.ValueOf(updateData).Elem()

    fields := tabler.GetStructInfo(updateData)
    updateValues := make(map[string]interface{})

    for i, field := range fields.Fields {
        if field == "ID" || reflect.DeepEqual(v.Field(i).Interface(), reflect.Zero(v.Field(i).Type()).Interface()) {
            continue
        }
        updateValues[field] = v.Field(i).Interface()
    }

    sql, values, err := p.SqlBuilder.Update(updateData.TableName()).SetMap(updateValues).Where(sq.Eq{"username": username}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, values...)
    if err != nil {
        return err
    }

    return nil
}

func (p PostgresUserRepository) DeleteByUsername(ctx context.Context, username string) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()
    sql, args, err := p.SqlBuilder.Delete("*").From("users").Where(sq.Eq{"username": username}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, args...)
    if err != nil {
        return err
    }
    return nil
}

func (p PostgresUserRepository) Login(ctx context.Context, username, token string) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    sql, values, err := p.SqlBuilder.Update("users").Set("token", token).Where(sq.Eq{"username": username}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql, values...)
    if err != nil {
        return err
    }
    return nil
}

func (p PostgresUserRepository) Logout(ctx context.Context, token string) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    sql, _, err := p.SqlBuilder.Update("users").Set("token", "").Where(sq.Eq{"token": token}).ToSql()
    if err != nil {
        return err
    }

    _, err = p.db.ExecContext(ctx, sql)
    if err != nil {
        return err
    }
    return nil
}

func (p PostgresUserRepository) CreateUser(ctx context.Context, user *entities.User) error {
    p.Mutex.Lock()
    defer p.Mutex.Unlock()

    v := reflect.ValueOf(user).Elem()

    values := []interface{}{}
    columns := []string{}
    for i := 0; i < v.NumField(); i++ {
        field := v.Type().Field(i)
        tag := field.Tag.Get("db")
        if tag == "id" || field.Name == "ID" {
            continue
        }
        values = append(values, v.Field(i).Interface())
        columns = append(columns, tag)
    }

    q, _, _ := p.SqlBuilder.Insert(user.TableName()).Columns(columns...).Values(values...).ToSql()
    _, err := p.db.ExecContext(ctx, q, values...)
    if err != nil {
        return err
    }

    return nil
}
