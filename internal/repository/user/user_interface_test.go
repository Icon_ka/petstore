package user

import (
    "testing"
    sqlmock "github.com/zhashkevych/go-sqlxmock"
    "petstore/internal/entities"
    "log"
    "context"
    "database/sql/driver"
    "regexp"
    "strings"
)

func TestPostgresUserRepository_CreateUser(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()
    user := &entities.User{
        ID:       1,
        Username: "testusername",
    }

    mock.ExpectExec("^INSERT INTO users").WithArgs(user.Username, user.FirstName, user.LastName, user.Email, user.Password, user.Phone, user.UserStatus, user.Token).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresUserRepository(db)

    err = p.CreateUser(ctx, user)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresUserRepository_CreateWithArray(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()
    users := []*entities.User{
        {ID: 1,
            Username: "testusername"},
    }

    mock.ExpectExec("^INSERT INTO users").WithArgs().WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresUserRepository(db)

    err = p.CreateWithArray(ctx, users)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresUserRepository_DeleteByUsername(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()

    mock.ExpectExec("^DELETE FROM users").WithArgs("test").WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresUserRepository(db)

    err = p.DeleteByUsername(ctx, "test")
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresUserRepository_GetByUsername(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        t.Fatalf("An error occurred while creating mock: %s", err)
    }
    defer db.Close()

    ctx := context.Background()

    rows := sqlmock.NewRows([]string{"id", "username", "first_name"}).
        AddRow(1, "test", "test_name")
    mock.ExpectQuery("^SELECT (.+) FROM users WHERE username = ?").WithArgs("test").WillReturnRows(rows)

    p := NewPostgresUserRepository(db)

    user, err := p.GetByUsername(ctx, "test")
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if len(user) != 1 {
        t.Fatalf("Expected 1 user, got %d", len(user))
    }

    if user[0].ID != 1 || user[0].Username != "test" || user[0].FirstName != "test_name" {
        t.Fatalf("Expected user with ID 1, name 'test', and FirstName 'test_name', got %+v", user[0])
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}

func TestPostgresUserRepository_Login(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    p := NewPostgresUserRepository(db)

    username := "testuser"
    token := "testtoken"
    sql := "UPDATE users SET token = $1 WHERE username = $2"
    sql = strings.TrimSpace(sql)
    sql = regexp.QuoteMeta(sql)

    args := []driver.Value{token, username}

    result := sqlmock.NewResult(1, 1)

    mock.ExpectExec(sql).WithArgs(args...).WillReturnResult(result)

    err = p.Login(context.Background(), username, token)
    if err != nil {
        t.Errorf("error was not expected while logging in: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("there were unfulfilled expectations: %s", err)
    }
}

func TestPostgresUserRepository_Logout(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
    }
    defer db.Close()

    repo := NewPostgresUserRepository(db)

    token := "testtoken"
    sql := "UPDATE users SET token = $1 WHERE token = $2"
    sql = strings.TrimSpace(sql)

    args := []driver.Value{token, token}

    result := sqlmock.NewResult(1, 1)

    mock.ExpectExec(sql).WithArgs(args...).WillReturnResult(result)

    err = repo.Logout(context.Background(), token)
    if err != nil {
        t.Errorf("error was not expected while logging out: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("there were unfulfilled expectations: %s", err)
    }
}

func TestPostgresUserRepository_UpdateByUsername(t *testing.T) {
    db, mock, err := sqlmock.Newx()
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    ctx := context.Background()

    ud := &entities.User{
        ID:         1,
        Username:   "dasd",
        FirstName:  "dasd",
        LastName:   "sadd",
        Email:      "",
        Password:   "",
        Phone:      "",
        UserStatus: 0,
        Token:      "",
    }
    mock.ExpectExec("^UPDATE users").WithArgs(ud.FirstName, ud.ID, ud.LastName, ud.Username, ud.Username).WillReturnResult(sqlmock.NewResult(1, 1))

    p := NewPostgresUserRepository(db)

    err = p.UpdateByUsername(ctx, "dasd", ud)
    if err != nil {
        t.Fatalf("Unexpected error: %s", err)
    }

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("There were unfulfilled expectations: %s", err)
    }
}
