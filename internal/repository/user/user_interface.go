package user

import (
    "context"
    "petstore/internal/entities"
)

type UserRepository interface {
    CreateWithArray(ctx context.Context, users []*entities.User) error
    GetByUsername(ctx context.Context, username string) ([]entities.User, error)
    UpdateByUsername(ctx context.Context, username string, updateData *entities.User) error
    DeleteByUsername(ctx context.Context, username string) error
    Login(ctx context.Context, username, token string) error
    Logout(ctx context.Context, token string) error
    CreateUser(ctx context.Context, users *entities.User) error
}
