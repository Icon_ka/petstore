package router

import (
    "github.com/go-chi/chi"

    "net/http"
    "petstore/internal/controller"
    "go.uber.org/zap"
)

func NewApiRouter(controllers *controller.Controllers) http.Handler {
    r := chi.NewRouter()

    r.Use(LoggerMiddleware)

    r.Route("/api", func(r chi.Router) {
        r.Post("/pet", controllers.PetCtrl.Create)
        r.Put("/pet", controllers.PetCtrl.Update)
        r.Get("/pet/findByStatus", controllers.PetCtrl.GetByStatus)
        r.Get("/pet/{petId}", controllers.PetCtrl.GetById)
        r.Put("/pet/{petId}", controllers.PetCtrl.UpdateById)
        r.Delete("/pet/{petId}", controllers.PetCtrl.DeleteById)

        r.Route("/store", func(r chi.Router) {
            r.Post("/order", controllers.StoreCtrl.Create)
            r.Get("/order/{OrderId}", controllers.StoreCtrl.GetByID)
            r.Delete("/order/{OrderId}", controllers.PetCtrl.DeleteById)
        })

        r.Route("/user", func(r chi.Router) {
            r.Get("/{username}", controllers.User.GetByUsername)
            r.Put("/{username}", controllers.User.UpdateByUsername)
            r.Delete("/{username}", controllers.User.DeleteByUsername)
            r.Post("/login", controllers.User.Login)
            r.Post("/logout", controllers.User.Logout)
            r.Post("/createWithArray", controllers.User.CreateWithArray)
            r.Post("/", controllers.User.CreateUser)
        })
    })

    return r
}

func LoggerMiddleware(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        logger, _ := zap.NewProduction()
        defer logger.Sync()

        logger.Info("Request received",
            zap.String("path", r.URL.Path),
            zap.String("method", r.Method),
            zap.String("ip", r.RemoteAddr),
        )

        next.ServeHTTP(w, r)
    })
}
