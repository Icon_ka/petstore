package store

import (
    "testing"
    "petstore/internal/entities"
    "github.com/stretchr/testify/mock"
    "context"
    "net/http"
    "net/http/httptest"
    "bytes"
    "encoding/json"
    "reflect"
)

type MockDB struct {
    mock.Mock
}

func (m *MockDB) Create(ctx context.Context, order entities.Order) error {
    args := m.Called(ctx, order)
    return args.Error(0)
}

func (m *MockDB) GetByID(ctx context.Context, id int64) ([]entities.Order, error) {
    args := m.Called(ctx, id)
    return args.Get(0).([]entities.Order), args.Error(1)
}

func (m *MockDB) DeleteByID(ctx context.Context, id int64) error {
    args := m.Called(ctx, id)
    return args.Error(0)
}

func TestStoreController_Create(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("Create", mock.Anything, mock.Anything).Return(nil)

    // Create a new PetController with the mock database
    p := StoreController{pg: db}

    // Create a new PetElement
    pet := entities.Pet{
        Name:   "Test Pet",
        Status: "Available",
    }

    // Marshal the PetElement into JSON
    jsonData, _ := json.Marshal(pet)

    // Create a new request
    req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonData))
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the Create method
    p.Create(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := "Заказ успешно создан"
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestStoreController_DeleteByID(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("DeleteByID", mock.Anything, mock.Anything).Return(nil)

    // Create a new PetController with the mock database
    p := StoreController{pg: db}

    // Create a new request
    req, err := http.NewRequest("DELETE", "/pet/1", nil)
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the Create method
    p.DeleteByID(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := "Заказ успешно удален"
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestStoreController_GetByID(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("GetByID", mock.Anything, int64(1)).Return([]entities.Order{{PetID: 1, Status: "Available"}}, nil)

    // Create a new PetController with the mock database
    p := StoreController{pg: db}

    // Create a new request
    req, err := http.NewRequest("GET", "/1", nil)
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the GetById method
    p.GetByID(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := []entities.Order{{PetID: 1, Status: "Available"}}
    if reflect.DeepEqual(rr.Body, expected) {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}
