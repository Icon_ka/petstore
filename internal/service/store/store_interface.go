package store

import "net/http"

type StAPI interface {
    Create(http.ResponseWriter, *http.Request)
    GetByID(http.ResponseWriter, *http.Request)
    DeleteByID(http.ResponseWriter, *http.Request)
}
