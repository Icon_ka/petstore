package store

import (
    "petstore/internal/repository/store"
    "net/http"
    "petstore/internal/entities"
    "encoding/json"
    "context"
    "fmt"
    "strconv"
    "strings"
)

type StoreController struct {
    pg store.StoreRepository
}

func NewStoreController(storeRepo store.StoreRepository) *StoreController {
    return &StoreController{pg: storeRepo}
}

func (s StoreController) Create(writer http.ResponseWriter, request *http.Request) {
    var order entities.Order
    err := json.NewDecoder(request.Body).Decode(&order)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    err = s.pg.Create(context.Background(), order)
    if err != nil {
        http.Error(writer, "Ошибка при создании заказа", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Заказ успешно создан")
}

func (s StoreController) GetByID(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    id, err := strconv.Atoi(parts[len(parts)-1])
    if err != nil {
        http.Error(writer, "Неверный формат ID", http.StatusBadRequest)
        return
    }

    order, err := s.pg.GetByID(context.Background(), int64(id))
    if err != nil {
        http.Error(writer, "Заказ не найден", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, order)
}

func (s StoreController) DeleteByID(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    id, err := strconv.Atoi(parts[len(parts)-1])
    if err != nil {
        http.Error(writer, "Неверный формат ID", http.StatusBadRequest)
        return
    }

    err = s.pg.DeleteByID(context.Background(), int64(id))
    if err != nil {
        http.Error(writer, "Заказ не найден", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, "Заказ успешно удален")
}
