package pet

import (
    "petstore/internal/repository/pet"
    "net/http"
    "petstore/internal/entities"
    "encoding/json"
    "context"
    "fmt"
    "strings"
    "strconv"
)

type PetController struct {
    pg pet.PetRepository
}

func NewPetController(petRepo pet.PetRepository) *PetController {
    return &PetController{pg: petRepo}
}

func (p PetController) Create(writer http.ResponseWriter, request *http.Request) {
    var petReq entities.Pet
    err := json.NewDecoder(request.Body).Decode(&petReq)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    err = p.pg.Create(context.Background(), &petReq)
    if err != nil {
        http.Error(writer, "Ошибка при создании животного", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Животное успешно создано")
}

func (p PetController) Update(writer http.ResponseWriter, request *http.Request) {
    var petReq entities.Pet
    err := json.NewDecoder(request.Body).Decode(&petReq)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    err = p.pg.Update(context.Background(), &petReq)
    if err != nil {
        http.Error(writer, "Ошибка при обновлении данных животного", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Данные животного успешно обновлены")
}

func (p PetController) GetByStatus(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    status := parts[len(parts)-1]

    pet, err := p.pg.GetByStatus(context.Background(), status)
    if err != nil {
        http.Error(writer, "Животное не найдено", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, pet)
}

func (p PetController) GetById(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    id, err := strconv.Atoi(parts[len(parts)-1])
    if err != nil {
        http.Error(writer, "Неверный формат ID", http.StatusBadRequest)
        return
    }

    pet, err := p.pg.GetById(context.Background(), int64(id))
    if err != nil {
        http.Error(writer, "Животное не найдено", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, pet)
}

func (p PetController) UpdateById(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    id, err := strconv.Atoi(parts[len(parts)-1])
    if id <= 0 {
        http.Error(writer, "Неверный формат ID", http.StatusBadRequest)
        return
    }

    var petReq entities.Pet
    err = json.NewDecoder(request.Body).Decode(&petReq)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    err = p.pg.UpdateById(context.Background(), int64(id), petReq.Name, petReq.Status)
    if err != nil {
        http.Error(writer, "Ошибка при обновлении данных животного", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Данные животного успешно обновлены")
}

func (p PetController) DeleteById(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    id, err := strconv.Atoi(parts[len(parts)-1])
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }
    if id <= 0 {
        http.Error(writer, "Неверный формат ID", http.StatusBadRequest)
        return
    }

    err = p.pg.DeleteById(context.Background(), int64(id))
    if err != nil {
        http.Error(writer, "Ошибка при удалении животного", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Животное успешно удалено")
}
