package pet

import "net/http"

type PetAPI interface {
    Create(http.ResponseWriter, *http.Request)
    Update(http.ResponseWriter, *http.Request)
    GetByStatus(http.ResponseWriter, *http.Request)
    GetById(http.ResponseWriter, *http.Request)
    UpdateById(http.ResponseWriter, *http.Request)
    DeleteById(http.ResponseWriter, *http.Request)
}
