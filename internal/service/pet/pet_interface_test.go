package pet

import (
    "bytes"
    "encoding/json"
    "net/http"
    "net/http/httptest"
    "testing"

    "github.com/stretchr/testify/mock"

    "petstore/internal/entities"
    "context"
    "reflect"
)

type MockDB struct {
    mock.Mock
}

func (m *MockDB) Create(ctx context.Context, pet *entities.Pet) error {
    args := m.Called(ctx, pet)
    return args.Error(0)
}

func (m *MockDB) Update(ctx context.Context, pet *entities.Pet) error {
    args := m.Called(ctx, pet)
    return args.Error(0)
}

func (m *MockDB) GetByStatus(ctx context.Context, status string) ([]entities.Pet, error) {
    args := m.Called(ctx, status)
    return args.Get(0).([]entities.Pet), args.Error(1)
}

func (m *MockDB) GetById(ctx context.Context, id int64) ([]entities.Pet, error) {
    args := m.Called(ctx, id)
    return args.Get(0).([]entities.Pet), args.Error(1)
}

func (m *MockDB) UpdateById(ctx context.Context, id int64, name string, status string) error {
    args := m.Called(ctx, id, name, status)
    return args.Error(0)
}

func (m *MockDB) DeleteById(ctx context.Context, id int64) error {
    args := m.Called(ctx, id)
    return args.Error(0)
}

func TestCreate(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("Create", mock.Anything, mock.Anything).Return(nil)

    // Create a new PetController with the mock database
    p := PetController{pg: db}

    // Create a new PetElement
    pet := entities.Pet{
        Name:   "Test Pet",
        Status: "Available",
    }

    // Marshal the PetElement into JSON
    jsonData, _ := json.Marshal(pet)

    // Create a new request
    req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonData))
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the Create method
    p.Create(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := "Животное успешно создано"
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestPetController_DeleteById(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("DeleteById", mock.Anything, mock.Anything).Return(nil)

    // Create a new PetController with the mock database
    p := PetController{pg: db}

    // Create a new request
    req, err := http.NewRequest("DELETE", "/pet/1", nil)
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the Create method
    p.DeleteById(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := "Животное успешно удалено"
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestPetController_GetById(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("GetById", mock.Anything, int64(1)).Return([]entities.Pet{{Name: "Test Pet", Status: "Available"}}, nil)

    // Create a new PetController with the mock database
    p := PetController{pg: db}

    // Create a new request
    req, err := http.NewRequest("GET", "/1", nil)
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the GetById method
    p.GetById(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := []entities.Pet{{Name: "Test Pet", Status: "Available"}}
    if reflect.DeepEqual(rr.Body, expected) {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestPetController_GetByStatus(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("GetByStatus", mock.Anything, "Available").Return([]entities.Pet{{Name: "Test Pet", Status: "Available"}}, nil)

    // Create a new PetController with the mock database
    p := PetController{pg: db}

    // Create a new request
    req, err := http.NewRequest("GET", "/Available", nil)
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the GetByStatus method
    p.GetByStatus(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := []entities.Pet{{Name: "Test Pet", Status: "Available"}}
    if reflect.DeepEqual(rr.Body, expected) {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestPetController_Update(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("Update", mock.Anything, mock.Anything).Return(nil)

    // Create a new PetController with the mock database
    p := PetController{pg: db}

    // Create a new PetElement
    pet := entities.Pet{
        Name:   "Test Pet",
        Status: "Available",
    }

    // Marshal the PetElement into JSON
    jsonData, _ := json.Marshal(pet)

    // Create a new request
    req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonData))
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the Create method
    p.Update(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := "Данные животного успешно обновлены"
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}

func TestPetController_UpdateById(t *testing.T) {
    // Create a new MockDB
    db := new(MockDB)

    // Define the expectations for the mock database
    db.On("UpdateById", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

    // Create a new PetController with the mock database
    p := PetController{pg: db}

    // Create a new PetElement
    pet := entities.Pet{
        Name:   "Test Pet",
        Status: "Available",
    }

    // Marshal the PetElement into JSON
    jsonData, _ := json.Marshal(pet)

    // Create a new request
    req, err := http.NewRequest("POST", "/3", bytes.NewBuffer(jsonData))
    if err != nil {
        t.Fatal(err)
    }

    // Create a ResponseRecorder
    rr := httptest.NewRecorder()

    // Call the UpdateById method
    p.UpdateById(rr, req)

    // Check the status code
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
    }

    // Check the response body
    expected := "Данные животного успешно обновлены"
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
    }

    // Check that the expectations were met
    db.AssertExpectations(t)
}
