package user

import (
    "petstore/internal/repository/user"
    "net/http"
    "petstore/internal/entities"
    "encoding/json"
    "context"
    "fmt"
    "strings"
    "github.com/go-chi/jwtauth"
)

type UserController struct {
    pg user.UserRepository
}

func NewUserController(userRepo user.UserRepository) *UserController {
    return &UserController{pg: userRepo}
}

func (u UserController) CreateWithArray(writer http.ResponseWriter, request *http.Request) {
    var users []*entities.User
    err := json.NewDecoder(request.Body).Decode(&users)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    err = u.pg.CreateWithArray(context.Background(), users)
    if err != nil {
        http.Error(writer, "Ошибка при создании пользователей", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Пользователи успешно созданы")
}

func (u UserController) GetByUsername(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    username := parts[len(parts)-1]

    if len(username) <= 0 {
        http.Error(writer, "Неверный формат имени", http.StatusBadRequest)
        return
    }

    fmt.Println("Handler: ", username, "\n")
    findUser, err := u.pg.GetByUsername(context.Background(), username)
    if err != nil {
        http.Error(writer, "Пользователь не найден", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, findUser)
}

func (u UserController) UpdateByUsername(writer http.ResponseWriter, request *http.Request) {
    var updateUser entities.User
    err := json.NewDecoder(request.Body).Decode(&updateUser)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    path := request.URL.Path
    parts := strings.Split(path, "/")
    username := parts[len(parts)-1]
    if len(username) <= 0 {
        http.Error(writer, "Неверный формат имени", http.StatusBadRequest)
        return
    }

    err = u.pg.UpdateByUsername(context.Background(), username, &updateUser)
    if err != nil {
        http.Error(writer, "Пользователь не найден", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, "Данные пользователя успешно обновлены")
}

func (u UserController) DeleteByUsername(writer http.ResponseWriter, request *http.Request) {
    path := request.URL.Path
    parts := strings.Split(path, "/")
    username := parts[len(parts)-1]
    if len(username) <= 0 {
        http.Error(writer, "Неверный формат имени", http.StatusBadRequest)
        return
    }

    err := u.pg.DeleteByUsername(context.Background(), username)
    if err != nil {
        http.Error(writer, "Ошибка при удалении пользователя", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Пользователь успешно удален")
}

func (u UserController) Login(writer http.ResponseWriter, request *http.Request) {
    var loginUser entities.User
    err := json.NewDecoder(request.Body).Decode(&loginUser)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }
    if loginUser.Username == "" {
        http.Error(writer, "Неверный формат имени", http.StatusBadRequest)
        return
    }

    tokenAuth := jwtauth.New("HS256", []byte("mysecretkey"), nil)

    _, token, err := tokenAuth.Encode(map[string]interface{}{"username": loginUser.Username})
    if err != nil {
        http.Error(writer, "Ошибка создания токена", http.StatusNotFound)
        return
    }

    err = u.pg.Login(context.Background(), loginUser.Username, token)
    if err != nil {
        http.Error(writer, "Ошибка регистрации пользователя", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, "Пользователь успешно зарегистрирован")
}

func (u UserController) Logout(writer http.ResponseWriter, request *http.Request) {
    var loginUser entities.User
    err := json.NewDecoder(request.Body).Decode(&loginUser)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }
    if loginUser.Token == "" {
        http.Error(writer, "Неверный формат токена", http.StatusBadRequest)
        return
    }

    err = u.pg.Logout(context.Background(), loginUser.Token)
    if err != nil {
        http.Error(writer, "Ошибка разрыва соединения", http.StatusNotFound)
        return
    }

    fmt.Fprint(writer, "Сессия завершена")
}

func (u UserController) CreateUser(writer http.ResponseWriter, request *http.Request) {
    var userCreate entities.User
    err := json.NewDecoder(request.Body).Decode(&userCreate)
    if err != nil {
        http.Error(writer, "Ошибка при декодировании JSON", http.StatusBadRequest)
        return
    }

    err = u.pg.CreateUser(context.Background(), &userCreate)
    if err != nil {
        http.Error(writer, "Ошибка при создании пользователя", http.StatusInternalServerError)
        return
    }

    fmt.Fprint(writer, "Пользователь успешно создан")
}
