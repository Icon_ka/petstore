package user

import "net/http"

type UserAPI interface {
    CreateWithArray(http.ResponseWriter, *http.Request)
    GetByUsername(http.ResponseWriter, *http.Request)
    UpdateByUsername(http.ResponseWriter, *http.Request)
    DeleteByUsername(http.ResponseWriter, *http.Request)
    Login(http.ResponseWriter, *http.Request)
    Logout(http.ResponseWriter, *http.Request)
    CreateUser(http.ResponseWriter, *http.Request)
}
