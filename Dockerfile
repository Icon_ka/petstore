# Используем официальный образ Go как базовый
FROM golang:1.19-alpine as builder

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем исходники приложения и .env файл в рабочую директорию
COPY . .
COPY .env .env

# Скачиваем все зависимости
RUN go mod tidy

# Собираем приложение
RUN go build -o main

# Начинаем новую стадию сборки на основе минимального образа
FROM alpine:latest

# Добавляем исполняемый файл из первой стадии в корневую директорию контейнера
COPY --from=builder /app/main /main

# Добавляем .env файл в корневую директорию контейнера
COPY --from=builder /app/.env /.env


# Открываем порт 8080
EXPOSE 8080

# Запускаем приложение
CMD ["/main"]